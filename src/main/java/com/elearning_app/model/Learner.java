package com.elearning_app.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "learner")
public class Learner {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int id;
	@Column
	private String first_name;
	@Column
	public ArrayList<Learner_Plan> getLearner_plans() {
		return learner_plans;
	}
	public void setLearner_plans(ArrayList<Learner_Plan> learner_plans) {
		this.learner_plans = learner_plans;
	}
	private String last_name;
	@ManyToMany
	private ArrayList<Learner_Plan> learner_plans = new ArrayList<>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	@Override
	public String toString() {
		return "Learner [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + "]";
	}
	
}
