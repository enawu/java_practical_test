package com.elearning_app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.elearning_app.model.Learner;

@RepositoryRestResource(collectionResourceRel="learners",path="learners")
public interface LearnerRepo extends JpaRepository<Learner, Integer> {

}
