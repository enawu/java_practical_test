/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : praticaltest1

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 18/09/2019 03:04:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for competence
-- ----------------------------
DROP TABLE IF EXISTS `competence`;
CREATE TABLE `competence`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competence` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 300 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of competence
-- ----------------------------
INSERT INTO `competence` VALUES (1, 'Sourcing');
INSERT INTO `competence` VALUES (2, 'Negotiation');
INSERT INTO `competence` VALUES (3, 'Legal');
INSERT INTO `competence` VALUES (4, 'Finance');
INSERT INTO `competence` VALUES (5, 'Cost Management');
INSERT INTO `competence` VALUES (6, 'Operational Procurement');
INSERT INTO `competence` VALUES (7, 'Contract Management');
INSERT INTO `competence` VALUES (8, 'Category Management');
INSERT INTO `competence` VALUES (9, 'Strategy');
INSERT INTO `competence` VALUES (10, 'Project Mgmt');
INSERT INTO `competence` VALUES (11, 'Inventory Control');
INSERT INTO `competence` VALUES (12, 'Supply Planning');
INSERT INTO `competence` VALUES (13, 'Demand Planning');
INSERT INTO `competence` VALUES (14, 'Transportation');
INSERT INTO `competence` VALUES (15, 'Order Management');
INSERT INTO `competence` VALUES (16, 'Warehousing');
INSERT INTO `competence` VALUES (17, 'Manufacturing');
INSERT INTO `competence` VALUES (18, 'Procurement');
INSERT INTO `competence` VALUES (19, 'Legal - Contracting');
INSERT INTO `competence` VALUES (20, 'Financials');
INSERT INTO `competence` VALUES (21, 'Project Management');
INSERT INTO `competence` VALUES (22, 'Leadership');
INSERT INTO `competence` VALUES (23, 'Interpersonal');
INSERT INTO `competence` VALUES (24, 'Supplier Relationship Mgmt');
INSERT INTO `competence` VALUES (25, 'Business Partnering');
INSERT INTO `competence` VALUES (26, 'Generic');
INSERT INTO `competence` VALUES (27, 'Certificate');
INSERT INTO `competence` VALUES (28, 'Strategy and Analytics');
INSERT INTO `competence` VALUES (29, 'Accident Investigation - Safety');
INSERT INTO `competence` VALUES (30, 'Assets Management - RPM Management');
INSERT INTO `competence` VALUES (31, 'Assets Management - Transportation');
INSERT INTO `competence` VALUES (32, 'Assets Management - Warehousing');
INSERT INTO `competence` VALUES (33, 'Auditing - Safety');
INSERT INTO `competence` VALUES (34, 'Autonomous Maintenance - Brewing/Packaging/Utilities Operational');
INSERT INTO `competence` VALUES (35, 'Autonomous Maintenance Support - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (36, 'Brewing Equipment Operation & Control - Brewing Operational');
INSERT INTO `competence` VALUES (37, 'Brewing Quality Management & Control - Brewing Managerial');
INSERT INTO `competence` VALUES (38, 'Brewing Quality Management & Control - Brewing Operational');
INSERT INTO `competence` VALUES (39, 'Commercial & Financial Confidence - Customer Service');
INSERT INTO `competence` VALUES (40, 'Commercial Understanding - Demand Planning');
INSERT INTO `competence` VALUES (41, 'Commercial Understanding - Logistics Management');
INSERT INTO `competence` VALUES (42, 'Commercial Understanding - New Product Introduction');
INSERT INTO `competence` VALUES (43, 'Commercial Understanding - Sales & Operations Planning');
INSERT INTO `competence` VALUES (44, 'Commercial Understanding - Supply Chain Planning');
INSERT INTO `competence` VALUES (45, 'Complaints Management - Quality Managerial');
INSERT INTO `competence` VALUES (46, 'Cost Management - Brewery Management');
INSERT INTO `competence` VALUES (47, 'Customer Service Collaboration - Customer Service');
INSERT INTO `competence` VALUES (48, 'Customer Service Operations - Customer Service');
INSERT INTO `competence` VALUES (49, 'Commercial Understanding - RPM Management');
INSERT INTO `competence` VALUES (50, 'Cost Management - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (51, 'Cost Management - Quality Managerial');
INSERT INTO `competence` VALUES (52, 'Customer Insight & Market Knowledge - Customer Service');
INSERT INTO `competence` VALUES (53, 'Customer Service Strategy - Customer Service');
INSERT INTO `competence` VALUES (54, 'Demand Modelling - Demand Planning');
INSERT INTO `competence` VALUES (55, 'End to End Supply Chain Understanding - Customer Service');
INSERT INTO `competence` VALUES (56, 'Engineering Project Management - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (57, 'Environment & Sustainability - Brewery Management');
INSERT INTO `competence` VALUES (58, 'Equipment Maintenance - Brewing/Packaging/Utilities Operational');
INSERT INTO `competence` VALUES (59, 'Excellence in Analysis - Quality Operational');
INSERT INTO `competence` VALUES (60, 'Excellence in Execution - Logistics Management');
INSERT INTO `competence` VALUES (61, 'Excellence in Execution - New Product Introduction');
INSERT INTO `competence` VALUES (62, 'Excellence in Execution - Production Scheduling');
INSERT INTO `competence` VALUES (63, 'Excellence in Execution - RPM Management');
INSERT INTO `competence` VALUES (64, 'Excellence in Execution - Sales & Operations Planning');
INSERT INTO `competence` VALUES (65, 'Excellence in Execution - Supply Chain Planning');
INSERT INTO `competence` VALUES (66, 'Excellence in Execution - Supply Planning');
INSERT INTO `competence` VALUES (67, 'Excellence in Execution - Transportation');
INSERT INTO `competence` VALUES (68, 'Excellence in Execution - Warehousing');
INSERT INTO `competence` VALUES (69, 'Financial Understanding - Demand Planning');
INSERT INTO `competence` VALUES (70, 'Financial Understanding - Logistics Management');
INSERT INTO `competence` VALUES (71, 'Financial Understanding - New Product Introduction');
INSERT INTO `competence` VALUES (72, 'Financial Understanding - RPM Management');
INSERT INTO `competence` VALUES (73, 'Financial Understanding - Sales & Operations Planning');
INSERT INTO `competence` VALUES (74, 'Financial Understanding - Supply Chain Planning');
INSERT INTO `competence` VALUES (75, 'Financial Understanding - Supply Planning');
INSERT INTO `competence` VALUES (76, 'Integrated Business Understanding - Sales & Operations Planning');
INSERT INTO `competence` VALUES (77, 'Integrated Supply Chain Understanding - Demand Planning');
INSERT INTO `competence` VALUES (78, 'Integrated Supply Chain Understanding - New Product Introduction');
INSERT INTO `competence` VALUES (79, 'Integrated Supply Chain Understanding - Production Scheduling');
INSERT INTO `competence` VALUES (80, 'Integrated Supply Chain Understanding - Supply Chain Planning');
INSERT INTO `competence` VALUES (81, 'Integrated Supply Chain Understanding - Supply Planning');
INSERT INTO `competence` VALUES (82, 'Inventory Management - Warehousing');
INSERT INTO `competence` VALUES (83, 'Logistics Service Provider Contract Management - Logistics Management');
INSERT INTO `competence` VALUES (84, 'Logistics Strategy Development & Implementation - Logistics Management');
INSERT INTO `competence` VALUES (85, 'Maintenance Cost Management - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (86, 'Maintenance Engineering - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (87, 'Maintenance Execution Control - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (88, 'Maintenance Management - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (89, 'Maintenance Processes Management - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (90, 'Maintenance Strategy Development & Implementation - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (91, 'Management of Brewing Process & Execution - Brewing Managerial');
INSERT INTO `competence` VALUES (92, 'Management of Packaging Process & Execution - Packaging Managerial');
INSERT INTO `competence` VALUES (93, 'Management of Process, Performance and Excellence in Execution - Brewery Management');
INSERT INTO `competence` VALUES (94, 'Management of Safety Process & Systems - Safety');
INSERT INTO `competence` VALUES (95, 'Management of Utilities Processes & Operations - Utilities Managerial');
INSERT INTO `competence` VALUES (96, 'Management Reporting - Brewery Management');
INSERT INTO `competence` VALUES (97, 'Management Reporting - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (98, 'Management Reporting - Logistics Management');
INSERT INTO `competence` VALUES (99, 'Management Reporting - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (100, 'Management Reporting - New Product Introduction');
INSERT INTO `competence` VALUES (101, 'Management Reporting - Production Scheduling');
INSERT INTO `competence` VALUES (102, 'Management Reporting - Quality Managerial');
INSERT INTO `competence` VALUES (103, 'Management Reporting - RPM Management');
INSERT INTO `competence` VALUES (104, 'Management Reporting - Sales & Operations Planning');
INSERT INTO `competence` VALUES (105, 'Management Reporting - Transportation');
INSERT INTO `competence` VALUES (106, 'Management Reporting - Warehousing');
INSERT INTO `competence` VALUES (107, 'Operation & Control of Laboratory Environment & Equipment - Quality Operational');
INSERT INTO `competence` VALUES (108, 'Packaging Equipment Operation & Control - Packaging Operational');
INSERT INTO `competence` VALUES (109, 'Packaging Quality Management & Control Operator - Packaging Operational');
INSERT INTO `competence` VALUES (110, 'Packaging Quality Management & Control - Packaging Managerial');
INSERT INTO `competence` VALUES (111, 'Packaging Quality Management & Control Senior Operator - Packaging Operational');
INSERT INTO `competence` VALUES (112, 'Planned Maintenance Execution - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (113, 'Planning Process Management - Demand Planning');
INSERT INTO `competence` VALUES (114, 'Planning Process Management - Supply Chain Planning');
INSERT INTO `competence` VALUES (115, 'Planning Process Management - Supply Planning');
INSERT INTO `competence` VALUES (116, 'Portfolio Management - Brewery Management');
INSERT INTO `competence` VALUES (117, 'Process Support - Quality Operational');
INSERT INTO `competence` VALUES (118, 'Production Strategy Development & Implementation - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (119, 'Project Management - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (120, 'Project Management - Quality Managerial');
INSERT INTO `competence` VALUES (121, 'Quality Assurance - Quality Managerial');
INSERT INTO `competence` VALUES (122, 'Quality Assurance - Transportation');
INSERT INTO `competence` VALUES (123, 'Quality Systems - Quality Managerial');
INSERT INTO `competence` VALUES (124, 'Quality Systems & Procedures - Quality Operational');
INSERT INTO `competence` VALUES (125, 'Quality, Technology & Product Development - Quality Managerial');
INSERT INTO `competence` VALUES (126, 'Reporting - Brewing/Packaging/Utilities Operational');
INSERT INTO `competence` VALUES (127, 'Reporting - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (128, 'Reporting - Quality Operational');
INSERT INTO `competence` VALUES (129, 'Resource Planning - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (130, 'Resource Planning - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (131, 'Resource Planning - Quality Managerial');
INSERT INTO `competence` VALUES (132, 'Risk Assessment - Safety');
INSERT INTO `competence` VALUES (133, 'RPM Capacity Planning - RPM Management');
INSERT INTO `competence` VALUES (134, 'Safety - Brewery Management');
INSERT INTO `competence` VALUES (135, 'Safety - Brewing/Packaging/Utilities Managerial');
INSERT INTO `competence` VALUES (136, 'Safety - Brewing/Packaging/Utilities Operational');
INSERT INTO `competence` VALUES (137, 'Safety - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (138, 'Safety - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (139, 'Safety - Quality Managerial');
INSERT INTO `competence` VALUES (140, 'Safety - Quality Operational');
INSERT INTO `competence` VALUES (141, 'Safety Culture & Behavioural - Safety');
INSERT INTO `competence` VALUES (142, 'Safety Rules Standards and Compliance - Safety');
INSERT INTO `competence` VALUES (143, 'Safety Training, Coaching & Mentoring - Safety');
INSERT INTO `competence` VALUES (144, 'Schedule Optimization - Production Scheduling');
INSERT INTO `competence` VALUES (145, 'Scheduling Process Management - Production Scheduling');
INSERT INTO `competence` VALUES (146, 'Service Level Management - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (147, 'SHE & Sustainable - Transportation');
INSERT INTO `competence` VALUES (148, 'SHE & Sustainable - Warehousing');
INSERT INTO `competence` VALUES (149, 'Spare Parts Management - Maintenance & Engineering Managerial');
INSERT INTO `competence` VALUES (150, 'Spare Parts Stock-Keeping - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (151, 'Stakeholder Management - Brewery Management');
INSERT INTO `competence` VALUES (152, 'Stakeholder Management - New Product Introduction');
INSERT INTO `competence` VALUES (153, 'Stakeholder Management - Safety');
INSERT INTO `competence` VALUES (154, 'Stakeholder Management - Sales & Operations Planning');
INSERT INTO `competence` VALUES (155, 'Stock Quality Assurance - Warehousing');
INSERT INTO `competence` VALUES (156, 'Strategy Development & Implementation - Brewery Management');
INSERT INTO `competence` VALUES (157, 'Supply Chain Optimization - Supply Planning');
INSERT INTO `competence` VALUES (158, 'Sustainable Logistics - Logistics Management');
INSERT INTO `competence` VALUES (159, 'Tracking & Reporting - Safety');
INSERT INTO `competence` VALUES (160, 'Transportation Planning - Transportation');
INSERT INTO `competence` VALUES (161, 'Transportation Service Provider Management - Transportation');
INSERT INTO `competence` VALUES (162, 'Unplanned Maintenance Execution - Maintenance & Engineering Operational');
INSERT INTO `competence` VALUES (163, 'Utilities Equipment Operation & Control - Utilities Operational');
INSERT INTO `competence` VALUES (164, 'Utilities Quality Management & Control - Utilities Managerial');
INSERT INTO `competence` VALUES (165, 'Utilities Quality Management & Control - Utilities Operational');
INSERT INTO `competence` VALUES (166, 'Warehouse Planning - Warehousing');
INSERT INTO `competence` VALUES (167, 'Warehouse Service Provider Management - Warehousing');
INSERT INTO `competence` VALUES (168, 'Beschaffungsmarktanalysen');
INSERT INTO `competence` VALUES (169, 'Beschaffungsmanagement');
INSERT INTO `competence` VALUES (170, 'Produktkostenmanagement');
INSERT INTO `competence` VALUES (171, 'Lieferantenmanagement');
INSERT INTO `competence` VALUES (172, 'Warengruppenmanagement');
INSERT INTO `competence` VALUES (173, 'Fachführung');
INSERT INTO `competence` VALUES (174, 'Supply Chain Management');
INSERT INTO `competence` VALUES (175, 'Logistikmanagement');
INSERT INTO `competence` VALUES (176, 'Tender Management');
INSERT INTO `competence` VALUES (177, 'Communication & Negotiation Management');
INSERT INTO `competence` VALUES (178, 'Legal Aspects & Contract Management');
INSERT INTO `competence` VALUES (179, 'Finance in Procurement & Logistics');
INSERT INTO `competence` VALUES (180, 'Procurement Tools & Operational Purchasing');
INSERT INTO `competence` VALUES (181, 'Supplier Management');
INSERT INTO `competence` VALUES (182, 'Strategic Sourcing');
INSERT INTO `competence` VALUES (183, 'Logistics - Inventory Control');
INSERT INTO `competence` VALUES (184, 'Logistics - Supply Planning');
INSERT INTO `competence` VALUES (185, 'Logistics - Transportation');
INSERT INTO `competence` VALUES (186, 'Logistics - Order Management');
INSERT INTO `competence` VALUES (187, 'CRM');
INSERT INTO `competence` VALUES (188, 'Prospecting');
INSERT INTO `competence` VALUES (189, 'Active Listening');
INSERT INTO `competence` VALUES (190, 'Stakeholder Management');
INSERT INTO `competence` VALUES (191, 'Logistics Service Provider Contract Management');
INSERT INTO `competence` VALUES (192, 'Customer Service Strategy');
INSERT INTO `competence` VALUES (193, 'Management of Utilities Processes & Operations');
INSERT INTO `competence` VALUES (194, 'End to End Supply Chain Understanding');
INSERT INTO `competence` VALUES (195, 'Production Strategy Development & Implementation');
INSERT INTO `competence` VALUES (196, 'Commercial & Financial Confidence');
INSERT INTO `competence` VALUES (197, 'Transportation Service Provider Management');
INSERT INTO `competence` VALUES (198, 'Customer Insight & Market Knowledge');
INSERT INTO `competence` VALUES (199, 'Excellence in Analysis');
INSERT INTO `competence` VALUES (200, 'Inventory Management');
INSERT INTO `competence` VALUES (201, 'Resource Planning');
INSERT INTO `competence` VALUES (202, 'Schedule Optimization');
INSERT INTO `competence` VALUES (203, 'Transportation Planning');
INSERT INTO `competence` VALUES (204, 'Customer Service Operations');
INSERT INTO `competence` VALUES (205, 'Warehouse Planning');
INSERT INTO `competence` VALUES (206, 'Management of Packaging Process & Execution');
INSERT INTO `competence` VALUES (207, 'Supply Chain Optimization');
INSERT INTO `competence` VALUES (208, 'Environment & Sustainability');
INSERT INTO `competence` VALUES (209, 'Service Level Management');
INSERT INTO `competence` VALUES (210, 'Financial Understanding');
INSERT INTO `competence` VALUES (211, 'Getting Started');
INSERT INTO `competence` VALUES (212, 'Meeting Management');
INSERT INTO `competence` VALUES (213, 'Presenting');
INSERT INTO `competence` VALUES (214, 'Closing');
INSERT INTO `competence` VALUES (215, 'Sales Negotiations');
INSERT INTO `competence` VALUES (216, 'Quality Managerial');
INSERT INTO `competence` VALUES (217, 'Safety');
INSERT INTO `competence` VALUES (218, 'Operativer Einkauf');
INSERT INTO `competence` VALUES (219, 'SC Fundamentals');
INSERT INTO `competence` VALUES (221, 'Agreement and Contract Management');
INSERT INTO `competence` VALUES (222, 'Contracting & Governance');
INSERT INTO `competence` VALUES (223, 'Contracting Strategy Development & Negotiations');
INSERT INTO `competence` VALUES (224, 'Demand Management');
INSERT INTO `competence` VALUES (225, 'Effective Communication');
INSERT INTO `competence` VALUES (226, 'Finalizing Agreement and Contracts');
INSERT INTO `competence` VALUES (227, 'Forecasting & Inventory Planning');
INSERT INTO `competence` VALUES (228, 'Import and Export Legal and Tax Compliance');
INSERT INTO `competence` VALUES (229, 'Import and Export Regulations and Tax Laws');
INSERT INTO `competence` VALUES (230, 'Interpersonal & Cultural Sensitivity');
INSERT INTO `competence` VALUES (232, 'Logistics & Materials Movement');
INSERT INTO `competence` VALUES (233, 'Market Intelligence & Analysis');
INSERT INTO `competence` VALUES (234, 'Materials Issuing');
INSERT INTO `competence` VALUES (235, 'Materials Ordering & Expediting');
INSERT INTO `competence` VALUES (236, 'Materials Return');
INSERT INTO `competence` VALUES (237, 'Negotiation & Influencing');
INSERT INTO `competence` VALUES (238, 'Operational Excellence - Compliance');
INSERT INTO `competence` VALUES (239, 'Planning and Organizing');
INSERT INTO `competence` VALUES (240, 'Procurement & Category Strategy Development');
INSERT INTO `competence` VALUES (241, 'Procurement Process Development');
INSERT INTO `competence` VALUES (242, 'Requisitions and Purchase Orders');
INSERT INTO `competence` VALUES (243, 'RFQs & Tenders');
INSERT INTO `competence` VALUES (244, 'Risk Management');
INSERT INTO `competence` VALUES (245, 'Routine Warehouse and Yard Activities');
INSERT INTO `competence` VALUES (246, 'Scrap, Reject, Surplus and Obsolete Material');
INSERT INTO `competence` VALUES (247, 'Spend Analysis');
INSERT INTO `competence` VALUES (248, 'Stock Take, Reconciliation & disposal of slow Moving and Dead stock');
INSERT INTO `competence` VALUES (249, 'Strategic Procurement & Supply Chain Management');
INSERT INTO `competence` VALUES (250, 'Strategic Sourcing Plans');
INSERT INTO `competence` VALUES (251, 'Supplier Performance Measurement & Management');
INSERT INTO `competence` VALUES (252, 'Supplier Pricing Models');
INSERT INTO `competence` VALUES (253, 'Supplier Qualification, Assessment & Selection');
INSERT INTO `competence` VALUES (254, 'Contract Development & Set-up (including contractual risk mgmt.)');
INSERT INTO `competence` VALUES (255, 'Contract/ Purchase Terms & Conditions');
INSERT INTO `competence` VALUES (256, 'e-Sourcing Tools (RFI / RFP / RFQ / RFx)');
INSERT INTO `competence` VALUES (257, 'Managing Strategic Partnerships');
INSERT INTO `competence` VALUES (258, 'Market Intelligence (of product(s) / services(s) / Categories managed)');
INSERT INTO `competence` VALUES (259, 'Product Non-Conformance (NCR)');
INSERT INTO `competence` VALUES (260, 'Purchase Price Reduction');
INSERT INTO `competence` VALUES (261, 'Purchasing Rules');
INSERT INTO `competence` VALUES (262, 'Supplier Compliance (conduct, legal)');
INSERT INTO `competence` VALUES (263, 'Supplier Development');
INSERT INTO `competence` VALUES (264, 'Supplier Perfomance Management');
INSERT INTO `competence` VALUES (265, 'Supplier Rationalisation');
INSERT INTO `competence` VALUES (266, '(Cost/Spend/Financial) Analysis');
INSERT INTO `competence` VALUES (267, 'Capacity Management');
INSERT INTO `competence` VALUES (268, 'Code of Conduct');
INSERT INTO `competence` VALUES (269, 'Company Intellectual Property (IP) Guidelines');
INSERT INTO `competence` VALUES (270, 'Cost Reduction Through More Efficient Processes (indirect)');
INSERT INTO `competence` VALUES (271, 'Cross-Functional Teams');
INSERT INTO `competence` VALUES (272, 'Forecasting & Scheduling');
INSERT INTO `competence` VALUES (273, 'Forecasting and Scheduling');
INSERT INTO `competence` VALUES (274, 'INCOTERMS (Delivery Terms)');
INSERT INTO `competence` VALUES (275, 'Kanban / Consignment / Inventory Best Practices');
INSERT INTO `competence` VALUES (276, 'Logistics');
INSERT INTO `competence` VALUES (277, 'Ordering / Material Management (ERP / MRP)');
INSERT INTO `competence` VALUES (278, 'Planning (for every part)');
INSERT INTO `competence` VALUES (279, 'Supplier Assessment & Qualification (technical, quality, EHS elements)');
INSERT INTO `competence` VALUES (280, 'Supplier Audit (technical, quality, EHS)');
INSERT INTO `competence` VALUES (281, 'Supplier Data / Lead Time Accuracy');
INSERT INTO `competence` VALUES (282, 'Total Cost of Acquisition (TCA)');
INSERT INTO `competence` VALUES (283, 'Value Analysis / Value Engineering (VAVE) Initiatives (direct)');
INSERT INTO `competence` VALUES (284, 'Value Chain Excellence (VCE)');
INSERT INTO `competence` VALUES (285, 'ABCD Analysis');
INSERT INTO `competence` VALUES (286, 'e-Inventory Tools (RFS / MRP / 9 box)');
INSERT INTO `competence` VALUES (287, 'Inventory Inspection');
INSERT INTO `competence` VALUES (288, 'Inventory provision');
INSERT INTO `competence` VALUES (289, 'Inventory re-positioning / Sharing');
INSERT INTO `competence` VALUES (290, 'Stock accuracy');
INSERT INTO `competence` VALUES (291, 'Warehouse planning & Layout');
INSERT INTO `competence` VALUES (292, 'Change Management');
INSERT INTO `competence` VALUES (293, 'Responsible Sourcing');
INSERT INTO `competence` VALUES (294, 'Influencing & Collaboration');
INSERT INTO `competence` VALUES (295, 'Innovation & New Technologies');
INSERT INTO `competence` VALUES (296, 'Sourcing & Category Strategy');
INSERT INTO `competence` VALUES (297, 'Supplier Relationship Management');
INSERT INTO `competence` VALUES (298, 'Inspirational Leadership');
INSERT INTO `competence` VALUES (299, 'Sourcing Processes & Dependencies');

-- ----------------------------
-- Table structure for learner
-- ----------------------------
DROP TABLE IF EXISTS `learner`;
CREATE TABLE `learner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of learner
-- ----------------------------
INSERT INTO `learner` VALUES (1, 'Harris', 'Hooston');
INSERT INTO `learner` VALUES (2, 'John', 'Do');

-- ----------------------------
-- Table structure for learner_learner_plan
-- ----------------------------
DROP TABLE IF EXISTS `learner_learner_plan`;
CREATE TABLE `learner_learner_plan`  (
  `Learner_id` int(11) NOT NULL,
  `Learner_plan_id` int(255) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of learner_learner_plan
-- ----------------------------
INSERT INTO `learner_learner_plan` VALUES (1, 2);
INSERT INTO `learner_learner_plan` VALUES (1, 3);
INSERT INTO `learner_learner_plan` VALUES (1, 4);
INSERT INTO `learner_learner_plan` VALUES (1, 5);
INSERT INTO `learner_learner_plan` VALUES (1, 6);
INSERT INTO `learner_learner_plan` VALUES (1, 7);
INSERT INTO `learner_learner_plan` VALUES (1, 8);
INSERT INTO `learner_learner_plan` VALUES (1, 9);
INSERT INTO `learner_learner_plan` VALUES (1, 10);
INSERT INTO `learner_learner_plan` VALUES (1, 11);
INSERT INTO `learner_learner_plan` VALUES (2, 67);
INSERT INTO `learner_learner_plan` VALUES (2, 68);
INSERT INTO `learner_learner_plan` VALUES (2, 69);
INSERT INTO `learner_learner_plan` VALUES (2, 70);
INSERT INTO `learner_learner_plan` VALUES (2, 71);
INSERT INTO `learner_learner_plan` VALUES (2, 72);
INSERT INTO `learner_learner_plan` VALUES (2, 73);
INSERT INTO `learner_learner_plan` VALUES (2, 74);
INSERT INTO `learner_learner_plan` VALUES (2, 75);
INSERT INTO `learner_learner_plan` VALUES (2, 76);

-- ----------------------------
-- Table structure for learning_plan
-- ----------------------------
DROP TABLE IF EXISTS `learning_plan`;
CREATE TABLE `learning_plan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `learning_plan_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `curriculum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `curriculum_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `curriculum_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `competence_id` int(11) NOT NULL,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `learner_id` int(11) NOT NULL,
  `scheduled_start_date` date NULL DEFAULT NULL,
  `scheduled_end_date` date NULL DEFAULT NULL,
  `curriculum_status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `completion_date` datetime(0) NULL DEFAULT NULL,
  `credit_value` int(11) NULL DEFAULT NULL,
  `seat_time_min` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_learning_plan_1`(`learner_id`, `curriculum_code`, `competence_id`) USING BTREE,
  INDEX `fk_competence_learning_plan_1`(`competence_id`) USING BTREE,
  INDEX `fk_learner_learning_plan_1`(`learner_id`) USING BTREE,
  INDEX `learning_plan_name_index`(`learning_plan_name`) USING BTREE,
  INDEX `user_id_lp_type_name_index`(`learner_id`, `learning_plan_name`, `type`) USING BTREE,
  CONSTRAINT `fk_competence_learning_plan_1` FOREIGN KEY (`competence_id`) REFERENCES `competence` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of learning_plan
-- ----------------------------
INSERT INTO `learning_plan` VALUES (2, 'Packaging Manager Foundation', 'Heineken Getting Started', 'High Impact eLearning', '00HK001TR', 17, 'ILP', 1, '2019-01-24', '2019-06-23', 'Started', NULL, 15, 360);
INSERT INTO `learning_plan` VALUES (3, 'Packaging Manager Foundation', 'Supply Chain Strategy Part I - Key Capabilities', 'High Impact eLearning', 'B00A2SC153TR', 118, 'ILP', 1, '2019-09-07', '2019-09-22', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (4, 'Packaging Manager Foundation', 'Supply Chain Strategy Part I - Key Capabilities', 'High Impact eLearning', 'B00A2SC153TR', 163, 'ILP', 1, '2019-06-24', '2019-07-04', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (5, 'Packaging Manager Foundation', 'Supply Chain Strategy Part II - Key Decisions', 'High Impact eLearning', 'B00A2SC154TR', 118, 'ILP', 1, '2019-09-23', '2019-10-08', 'Not Started', NULL, 1, 25);
INSERT INTO `learning_plan` VALUES (6, 'Packaging Manager Foundation', 'Supply Chain Strategy Part II - Key Decisions', 'High Impact eLearning', 'B00A2SC154TR', 163, 'ILP', 1, '2019-07-05', '2019-07-15', 'Not Started', NULL, 1, 25);
INSERT INTO `learning_plan` VALUES (7, 'Packaging Manager Foundation', 'Getting Information from Big Data Part 1', 'High Impact eLearning', 'B00A3SC257TR', 92, 'ILP', 1, '2019-10-09', '2019-10-24', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (8, 'Packaging Manager Foundation', 'Getting Information from Big Data Part 1', 'High Impact eLearning', 'B00A3SC257TR', 126, 'ILP', 1, '2019-07-16', '2019-07-26', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (9, 'Packaging Manager Foundation', 'Data Preparation', 'High Impact eLearning', 'B00A3SC258TR', 126, 'ILP', 1, '2019-07-27', '2019-08-06', 'Not Started', NULL, 1, 25);
INSERT INTO `learning_plan` VALUES (10, 'Packaging Manager Foundation', 'Supply Chain Strategy in a Service Industry', 'Simulation', 'B00C2SC161', 118, 'ILP', 1, '2019-10-25', '2019-11-09', 'Not Started', NULL, 1, 10);
INSERT INTO `learning_plan` VALUES (11, 'Packaging Manager Foundation', 'Supply Chain Strategy in a Service Industry', 'Simulation', 'B00C2SC161', 163, 'ILP', 1, '2019-08-07', '2019-08-17', 'Not Started', NULL, 1, 10);
INSERT INTO `learning_plan` VALUES (67, 'Quality Assurance Manager Advance', 'Heineken Getting Started', 'High Impact eLearning', '00HK001TR', 17, 'ILP', 2, '2019-04-08', '2019-11-18', 'Started', NULL, 15, 360);
INSERT INTO `learning_plan` VALUES (68, 'Quality Assurance Manager Advance', 'Contract Management', 'Simulation', '07LXS3', 45, 'ILP', 2, '2019-11-19', '2019-12-04', 'Not Started', NULL, 1, 10);
INSERT INTO `learning_plan` VALUES (69, 'Quality Assurance Manager Advance', 'Getting Information from Big Data Part 1', 'High Impact eLearning', 'B00A3SC257TR', 102, 'ILP', 2, '2019-12-05', '2019-12-20', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (70, 'Quality Assurance Manager Advance', 'Data Preparation', 'High Impact eLearning', 'B00A3SC258TR', 102, 'ILP', 2, '2019-12-21', '2020-01-05', 'Not Started', NULL, 1, 25);
INSERT INTO `learning_plan` VALUES (71, 'Quality Assurance Manager Advance', 'Introduction to Supply Chain Planning', 'High Impact eLearning', 'B02A1SC151TR', 131, 'ILP', 2, '2020-01-06', '2020-01-21', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (72, 'Quality Assurance Manager Advance', 'S&OP Process', 'High Impact eLearning', 'B02A1SC253TR', 131, 'ILP', 2, '2020-01-22', '2020-02-06', 'Not Started', NULL, 1, 25);
INSERT INTO `learning_plan` VALUES (73, 'Quality Assurance Manager Advance', 'Master Scheduling I', 'High Impact eLearning', 'B02A1SC255TR', 131, 'ILP', 2, '2020-02-07', '2020-02-22', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (74, 'Quality Assurance Manager Advance', 'Master Scheduling II', 'High Impact eLearning', 'B02A1SC256TR', 131, 'ILP', 2, '2020-02-23', '2020-03-09', 'Not Started', NULL, 1, 20);
INSERT INTO `learning_plan` VALUES (75, 'Quality Assurance Manager Advance', 'Material Requirements Planning (MRP)', 'High Impact eLearning', 'B02A1SC258TR', 131, 'ILP', 2, '2020-03-10', '2020-03-25', 'Not Started', NULL, 1, 30);
INSERT INTO `learning_plan` VALUES (76, 'Quality Assurance Manager Advance', 'Supply Chain Planning For Mobile Phones', 'Simulation', 'B02C1SC352', 131, 'ILP', 2, '2020-03-26', '2020-04-11', 'Not Started', NULL, 1, 10);

SET FOREIGN_KEY_CHECKS = 1;
