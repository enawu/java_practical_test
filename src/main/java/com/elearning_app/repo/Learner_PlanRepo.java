package com.elearning_app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.elearning_app.model.Learner_Plan;

@RepositoryRestResource(collectionResourceRel="learner_plans",path="learner_plans")
public interface Learner_PlanRepo extends JpaRepository<Learner_Plan, Integer> {

}
