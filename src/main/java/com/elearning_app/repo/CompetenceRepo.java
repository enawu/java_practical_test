package com.elearning_app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.elearning_app.model.Competence;

@RepositoryRestResource(collectionResourceRel="competences",path="competences")
public interface CompetenceRepo extends JpaRepository<Competence, Integer> {

}
