package com.elearning_app.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElearningAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElearningAppApplication.class, args);
	}

}
