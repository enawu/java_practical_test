package com.elearning_app.model;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "learner_plan")
public class Learner_Plan {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String learning_plan_name;
	private String curriculum;
	private String curriculum_type;
	
	@Column(unique = true)
	private String curriculum_code;
	
	@Column(unique = true)
	@OneToOne
	private Competence competence;
	private String type;
	
	@Column(unique = true)
	@ManyToMany(mappedBy = "leaner_plans")
	private ArrayList<Learner> learners = new ArrayList<>();
	
	private java.sql.Date scheduled_start_date;
	private java.sql.Date scheduled_end_date;
	private String curriculum_status;
	private Timestamp  completion_date;
	private int credit_value;
	private int seat_time_min;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLearning_plan_name() {
		return learning_plan_name;
	}
	public void setLearning_plan_name(String learning_plan_name) {
		this.learning_plan_name = learning_plan_name;
	}
	public String getCurriculum() {
		return curriculum;
	}
	public void setCurriculum(String curriculum) {
		this.curriculum = curriculum;
	}
	public String getCurriculum_type() {
		return curriculum_type;
	}
	public void setCurriculum_type(String curriculum_type) {
		this.curriculum_type = curriculum_type;
	}
	public String getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(String curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public Competence getCompetence() {
		return competence;
	}
	public void setCompetence(Competence competence) {
		this.competence = competence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList<Learner> getLearners() {
		return learners;
	}
	public void setLearners(ArrayList<Learner> learners) {
		this.learners = learners;
	}
	public java.sql.Date getScheduled_start_date() {
		return scheduled_start_date;
	}
	public void setScheduled_start_date(java.sql.Date scheduled_start_date) {
		this.scheduled_start_date = scheduled_start_date;
	}
	public java.sql.Date getScheduled_end_date() {
		return scheduled_end_date;
	}
	public void setScheduled_end_date(java.sql.Date scheduled_end_date) {
		this.scheduled_end_date = scheduled_end_date;
	}
	public String getCurriculum_status() {
		return curriculum_status;
	}
	public void setCurriculum_status(String curriculum_status) {
		this.curriculum_status = curriculum_status;
	}
	public Timestamp getCompletion_date() {
		return completion_date;
	}
	public void setCompletion_date(Timestamp completion_date) {
		this.completion_date = completion_date;
	}
	public int getCredit_value() {
		return credit_value;
	}
	public void setCredit_value(int credit_value) {
		this.credit_value = credit_value;
	}
	public int getSeat_time_min() {
		return seat_time_min;
	}
	public void setSeat_time_min(int seat_time_min) {
		this.seat_time_min = seat_time_min;
	}
	@Override
	public String toString() {
		return "Learner_Plan [id=" + id + ", learning_plan_name=" + learning_plan_name + ", curriculum=" + curriculum
				+ ", curriculum_type=" + curriculum_type + ", curriculum_code=" + curriculum_code + ", competence="
				+ competence + ", type=" + type + ", learners=" + learners + ", scheduled_start_date="
				+ scheduled_start_date + ", scheduled_end_date=" + scheduled_end_date + ", curriculum_status="
				+ curriculum_status + ", completion_date=" + completion_date + ", credit_value=" + credit_value
				+ ", seat_time_min=" + seat_time_min + "]";
	}
	
	
	
	
}
